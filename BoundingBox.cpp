//
// Created by razvan on 1/5/23.
//
#include <vector>

#include "BoundingBox.hpp"

namespace gps {

    static bool isVertexInSet(const Vertex &v, const std::vector<Vertex> &vertices)
    {
        for (Vertex vn : vertices)
            if (v.Normal == vn.Normal)
                return true;
        return false;
    }

    static std::vector<Vertex> minSetOfVertices(const Mesh &mesh)
    {
        std::vector<Vertex> vertices;
        for (Vertex v : mesh.vertices)
            if (!isVertexInSet(v, vertices))
                vertices.push_back(v);
        return vertices;
    }

    static bool isPointBehindPlane(glm::vec3 point, glm::vec3 normal, glm::vec3 vertex)
    {
        GLfloat angle = glm::dot(glm::normalize(normal), glm::normalize(vertex - point));
        return angle > glm::cos(glm::radians(90.0f));
    }

    static bool isPointInMesh(const std::vector<Vertex> &vertices, glm::vec3 point)
    {
        for (Vertex v : vertices)
            if (!isPointBehindPlane(point, v.Normal, v.Position))
                return false;
        return true;
    }

    bool BoundingBox::isPointInBoundingBox(glm::vec3 point)
    {
        for (const Mesh &mesh: this->getMeshes())
            if (isPointInMesh(minSetOfVertices(mesh), point))
                return true;
        return false;
    }

}