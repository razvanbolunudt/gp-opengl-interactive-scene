#ifndef BoundingBox_hpp
#define BoundingBox_hpp

#include "Model3D.hpp"

#include <glm/glm.hpp>
//#include <iostream>
//#include <string>
//#include <vector>

namespace gps {

    class BoundingBox : public Model3D
    {
    public:
        bool isPointInBoundingBox(glm::vec3 point);
    };

}

#endif /* BoundingBox_hpp */
