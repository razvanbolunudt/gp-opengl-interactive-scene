#include "Camera.hpp"

#include <utility>

namespace gps {

    //Camera constructor
    Camera::Camera(glm::vec3 cameraPosition, glm::vec3 cameraTarget, glm::vec3 cameraUp, BoundingBox box) {
        this->cameraPosition = cameraPosition;
        this->cameraTarget = cameraTarget;
        this->cameraUpDirection = cameraUp;
        this->cameraFrontDirection = glm::normalize(this->cameraTarget - this->cameraPosition);
        this->cameraRightDirection = glm::normalize(glm::cross(this->cameraFrontDirection, this->cameraUpDirection));
        this->fly = false;
        this->boundingBox = std::move(box);
    }

    //return the view matrix, using the glm::lookAt() function
    glm::mat4 Camera::getViewMatrix() {
        return glm::lookAt(
                cameraPosition,
                cameraPosition+cameraFrontDirection,
                glm::normalize(cameraUpDirection)
        );
    }

    static glm::vec3 removeHeightCoord(glm::vec3 vector) {
        return {vector.x, 0.0f, vector.z};
    }

    //update the camera internal parameters following a camera move event
    void Camera::move(MOVE_DIRECTION direction, float speed) {
        glm::vec3 nextPosition;
        switch (direction) {
            case MOVE_FORWARD:
                nextPosition = cameraPosition
                        + (speed * ((fly) ? cameraFrontDirection : removeHeightCoord(cameraFrontDirection)));
                break;
            case MOVE_BACKWARD:
                nextPosition = cameraPosition
                        - (speed * ((fly) ? cameraFrontDirection : removeHeightCoord(cameraFrontDirection)));
                break;
            case MOVE_LEFT:
                nextPosition = cameraPosition
                        - (speed * ((fly) ? cameraFrontDirection : removeHeightCoord(cameraRightDirection)));
                break;
            case MOVE_RIGHT:
                nextPosition = cameraPosition
                        + (speed * ((fly) ? cameraFrontDirection : removeHeightCoord(cameraRightDirection)));
                break;
        }
        if (boundingBox.isPointInBoundingBox(nextPosition))
            return;
        cameraPosition = nextPosition;
    }

    //update the camera internal parameters following a camera rotate event
    //yaw - camera rotation around the y axis
    //pitch - camera rotation around the x axis
    void Camera::rotate(float pitch, float yaw) {
        pitch = glm::radians(pitch);
        yaw = glm::radians(yaw);

        float x = glm::cos(pitch) * glm::cos(yaw);
        float y = glm::sin(pitch);
        float z = glm::sin(yaw) * glm::cos(pitch);

        this->cameraFrontDirection = glm::normalize(glm::vec3(x, y, z));
        this->cameraRightDirection = glm::normalize(glm::cross(this->cameraFrontDirection, this->cameraUpDirection));
    }

    glm::vec3 Camera::getCameraPosition() {
        return this->cameraPosition;
    }

    glm::vec3 Camera::getCameraFrontDirection() {
        return this->cameraTarget;
    }

    bool Camera::isCameraNear(glm::vec3 point, glm::vec3 delta)
    {
        glm::vec3 diff = cameraPosition - point;
        return (glm::abs(diff.x) < delta.x) && (glm::abs(diff.z) < delta.z);
    }

    static void moveToGround(glm::vec3 &cameraPosition) {
        glm::vec3 height = glm::vec3(0.0f, 2.0f - cameraPosition.y, 0.0f);
        cameraPosition += height;
    }

    void Camera::toggleFly() {
        fly = !fly;
        if (!fly)
            moveToGround(cameraPosition);
    }
}