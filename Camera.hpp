#ifndef Camera_hpp
#define Camera_hpp

#include "BoundingBox.hpp"

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

#include <string>

namespace gps {
    
    enum MOVE_DIRECTION {MOVE_FORWARD, MOVE_BACKWARD, MOVE_RIGHT, MOVE_LEFT};
    
    class Camera
    {
    public:
        //Camera constructors
        Camera() = default;
        Camera(glm::vec3 cameraPosition, glm::vec3 cameraTarget, glm::vec3 cameraUp, BoundingBox box);
        //return the view matrix, using the glm::lookAt() function
        glm::mat4 getViewMatrix();
        //update the camera internal parameters following a camera move event
        void move(MOVE_DIRECTION direction, float speed);
        //update the camera internal parameters following a camera rotate event
        //yaw - camera rotation around the y axis
        //pitch - camera rotation around the x axis
        void rotate(float pitch, float yaw);

        bool isCameraNear(glm::vec3 point, glm::vec3 delta);

        glm::vec3 getCameraPosition();
        glm::vec3 getCameraFrontDirection();
        void toggleFly();
        
    private:
        glm::vec3 cameraPosition{};
        glm::vec3 cameraTarget{};
        glm::vec3 cameraFrontDirection{};
        glm::vec3 cameraRightDirection{};
        glm::vec3 cameraUpDirection{};
        BoundingBox boundingBox;
        Model3D model;
        bool fly{};
    };
    
}

#endif /* Camera_hpp */
