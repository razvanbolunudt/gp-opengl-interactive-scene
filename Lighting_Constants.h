#ifndef OPENGL_PROJECT_CORE_LIGHTING_CONSTANTS_H
#define OPENGL_PROJECT_CORE_LIGHTING_CONSTANTS_H

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#define LEN_ARR(arr) (sizeof(arr)/sizeof(arr[0]))

#define LIGHTING_DIRECTIONAL ((GLint)1)
#define LIGHTING_POINTLIGHTS ((GLint)2)
#define LIGHTING_SPOTLIGHT ((GLint)3)

glm::vec3 pointLightPositions[] = {
        glm::vec3(88.474091, 7.344014, -28.061602),
        glm::vec3(88.188087, 7.344014, 72.935944),
        glm::vec3(1.097510, 7.344014, 121.155708),
        glm::vec3(-19.139330, 7.344014, 121.155708),
        glm::vec3(0.115812, 7.344014, -74.078415),
        glm::vec3(-86.612526, 7.344014, -27.115807),
        glm::vec3(-86.512100, 7.344014, 72.881027),
        glm::vec3(-20.376690, 7.344014, -74.078415)
};

glm::vec3 pointLightColors[] = {
        glm::vec3(255.0f, 255.0f, 255.0f),
        glm::vec3(255.0f, 255.0f, 255.0f),
        glm::vec3(255.0f, 255.0f, 255.0f),
        glm::vec3(255.0f, 255.0f, 255.0f),
        glm::vec3(255.0f, 255.0f, 255.0f),
        glm::vec3(255.0f, 255.0f, 255.0f),
        glm::vec3(255.0f, 255.0f, 255.0f),
        glm::vec3(255.0f, 255.0f, 255.0f)
};

GLfloat pointLightConsts[] = {
        1.0f,
        1.0f,
        1.0f,
        1.0f,
        1.0f,
        1.0f,
        1.0f,
        1.0f
};

GLfloat pointLightLinears[] = {
        0.7f,
        0.7f,
        0.7f,
        0.7f,
        0.7f,
        0.7f,
        0.7f,
        0.7f
};

GLfloat pointLightQuadratics[] = {
        1.8f,
        1.8f,
        1.8f,
        1.8f,
        1.8f,
        1.8f,
        1.8f,
        1.8f
};

glm::vec3 spotLightColor = glm::vec3(255.0f, 255.0f, 255.0f);
GLfloat spotLightConst = 1.0f;
GLfloat spotLightLinear = 0.7f;
GLfloat spotLightQuadratic = 1.80f;
GLfloat spotCutoff = glm::cos(glm::radians(12.5f));
GLfloat spotOuterCutoff = glm::cos(glm::radians(17.5f));

#endif //OPENGL_PROJECT_CORE_LIGHTING_CONSTANTS_H
