//
// Created by razvan on 1/14/23.
//
#include <GL/glew.h>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <cstdlib>

#include "Particle.h"
#include "Model3D.hpp"

namespace gps {

    ParticleSystem::ParticleSystem(int nParticles,
                                   glm::vec3 offset,
                                   glm::vec3 initParticleVelocity)
    {
        this->noParticles = nParticles;
        this->volumeOffset = offset;
        this->initialParticleVelocity = initParticleVelocity;

        InitParticles();
    }

    void ParticleSystem::InitParticles()
    {
        srand(0);
        for (int i=0; i<noParticles; ++i)
            particles.emplace_back(volumeOffset);
    }

    void ParticleSystem::SpawnNewParticles(int noNewParticles, float dt)
    {
        for (unsigned int i=0; i<noParticles; ++i) {
            Particle &p = particles[i];
            p.Life -= dt;
            if (p.Life > 0.0f)
                p.Position += p.Velocity * (dt);
        }

        for (unsigned int i=0; i<noNewParticles; ++i) {
            unsigned int particleIndex = FirstUnusedParticle();
            RespawnParticle(particles[particleIndex]);
        }
    }

    static GLfloat randGLfloat() {
        return ((GLfloat)(rand() % 100)) / 10.0f;
    }

    void ParticleSystem::RespawnParticle(Particle &particle)
    {
        particle.Position = glm::vec3(randGLfloat(), randGLfloat(), randGLfloat()) + volumeOffset;
        particle.Life = 1.0f;
        particle.Velocity = initialParticleVelocity;
    }

    unsigned int ParticleSystem::FirstUnusedParticle()
    {
        for (unsigned int i=lastUsedParticle; i<noParticles; ++i) {
            if (particles[i].Life <= 0.0f){
                lastUsedParticle = i;
                return i;
            }
        }

        for (unsigned int i=0; i<lastUsedParticle; ++i) {
            if (particles[i].Life <= 0.0f){
                lastUsedParticle = i;
                return i;
            }
        }

        lastUsedParticle = 0;
        return 0;
    }

    static glm::mat4 computeModelMatrix(Particle particle)
    {
        return glm::translate(glm::mat4(1.0f), particle.Position);
    }

    std::vector<ModelNormalMatrix> ParticleSystem::computeModelNormalMatrices(glm::mat4 view)
    {
        std::vector<ModelNormalMatrix> mats;

        for (Particle particle : particles) {
            glm::mat4 modelM = computeModelMatrix(particle);
            mats.emplace_back(modelM, glm::mat3(glm::inverseTranspose(view*modelM)));
        }

        return mats;
    }

}