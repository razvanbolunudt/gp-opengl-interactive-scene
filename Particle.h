//
// Created by razvan on 1/12/23.
//

#ifndef OPENGL_PROJECT_CORE_PARTICLE_H
#define OPENGL_PROJECT_CORE_PARTICLE_H

#include <glm/glm.hpp>
#include <GL/glut.h>
#include <vector>

#include "Shader.hpp"
#include "Model3D.hpp"

namespace gps {

    struct Particle
    {
        glm::vec3 Position;
        glm::vec3 Velocity;
        float Life;

        Particle(): Position(0.0f), Velocity(0.0f), Life(0.0f) { }
        Particle(glm::vec3 offset): Position(offset), Velocity(0.0f), Life(0.0f) { }
    };

    struct ModelNormalMatrix
    {
        glm::mat4 model;
        glm::mat4 normal;

        ModelNormalMatrix(): model(glm::mat4(1.0f)), normal(glm::mat4(1.0f)) { }
        ModelNormalMatrix(glm::mat4 m, glm::mat4 t): model(m), normal(t) { }
    };

    class ParticleSystem
    {
    public:
        ParticleSystem(int nParticles, glm::vec3 offset, glm::vec3 initParticleVelocity);

        void InitParticles();
        void SpawnNewParticles(int noNewParticles, float dt);

        void RespawnParticle(Particle &particle);

        std::vector<ModelNormalMatrix> computeModelNormalMatrices(glm::mat4 view);

    private:
        unsigned int FirstUnusedParticle();
        unsigned int lastUsedParticle = 0;

        int noParticles;
        std::vector<Particle> particles;
        glm::vec3 volumeOffset{};
        glm::vec3 initialParticleVelocity{};
    };

}

#endif //OPENGL_PROJECT_CORE_PARTICLE_H
