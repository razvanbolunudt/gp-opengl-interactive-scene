#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp> //core glm functionality
#include <glm/gtc/matrix_transform.hpp> //glm extension for generating common transformation matrices
#include <glm/gtc/matrix_inverse.hpp> //glm extension for computing inverse matrices
#include <glm/gtc/type_ptr.hpp> //glm extension for accessing the internal data structure of glm types

#include "Window.h"
#include "Shader.hpp"
#include "Camera.hpp"
#include "Model3D.hpp"
#include "BoundingBox.hpp"
#include "SkyBox.hpp"
#include "Particle.h"

#include "Lighting_Constants.h"

#include <iostream>

// window
gps::Window myWindow;

// matrices
glm::mat4 model;
glm::mat4 view;
glm::mat4 projection;
glm::mat3 normalMatrix;

// light parameters
glm::vec3 lightDir;
glm::vec3 lightColor;

GLint spotLightPositionLoc;
GLint spotLightDirectionLoc;
GLint lightingTypeLoc;

// shader uniform locations
GLint modelLoc;
GLint viewLoc;
GLint projectionLoc;
GLint normalMatrixLoc;
GLint lightDirLoc;
GLint lightColorLoc;

GLint lightingType = LIGHTING_DIRECTIONAL;

GLuint computeFog = 0;

// camera
gps::Camera myCamera;
GLfloat cameraSpeed = 0.1f;

GLboolean pressedKeys[1024];

// models
gps::Model3D scene;
GLfloat angle = 1.0f;

gps::Model3D planet;
GLfloat planetRotation;
GLfloat planetAngle;

gps::Model3D dor;
GLfloat dorRotation = 0.0f;

// bounding boxes
gps::BoundingBox boundingBox;

// shaders
gps::Shader myBasicShader;
gps::Shader depthMapShader;

// shadows
#define SHADOW_WIDTH 2048
#define SHADOW_HEIGHT 2048

GLuint shadowMapFBO;
GLuint depthMapTexture;

bool presentMode = false;

//skybox
gps::SkyBox mySkyBox;
gps::Shader skyboxShader;

// particle snow
gps::ParticleSystem particleSystem(
    2000,
    glm::vec3(14.0f, 0.0f, 0.0f),
    glm::vec3(0.0f, -1.0f, 0.0f)
);
gps::Model3D particle;
gps::Shader particleShader;

gps::Model3D boundingBoxMode;
bool drawBoundingBox = false;

GLenum glCheckError_(const char *file, int line)
{
	GLenum errorCode;
	while ((errorCode = glGetError()) != GL_NO_ERROR) {
		std::string error;
		switch (errorCode) {
            case GL_INVALID_ENUM:
                error = "INVALID_ENUM";
                break;
            case GL_INVALID_VALUE:
                error = "INVALID_VALUE";
                break;
            case GL_INVALID_OPERATION:
                error = "INVALID_OPERATION";
                break;
            case GL_STACK_OVERFLOW:
                error = "STACK_OVERFLOW";
                break;
            case GL_STACK_UNDERFLOW:
                error = "STACK_UNDERFLOW";
                break;
            case GL_OUT_OF_MEMORY:
                error = "OUT_OF_MEMORY";
                break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:
                error = "INVALID_FRAMEBUFFER_OPERATION";
                break;
        }
		std::cout << error << " | " << file << " (" << line << ")" << std::endl;
	}
	return errorCode;
}
#define glCheckError() glCheckError_(__FILE__, __LINE__)

void sendProjectionMatrixToShaders(glm::mat4 projMat) {
    myBasicShader.useShaderProgram();
    glUniformMatrix4fv(glGetUniformLocation(myBasicShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projMat));
    skyboxShader.useShaderProgram();
    glUniformMatrix4fv(glGetUniformLocation(skyboxShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projMat));
    particleShader.useShaderProgram();
    glUniformMatrix4fv(glGetUniformLocation(particleShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projMat));
}

void windowResizeCallback(GLFWwindow* window, int width, int height) {
	fprintf(stdout, "Window resized! New width: %d , and height: %d\n", width, height);
    projection = glm::perspective(glm::radians(45.0f), (float)width / (float)height, 0.1f, 200.0f);
    sendProjectionMatrixToShaders(projection);
    glViewport(0, 0, width, height);
    fprintf(stdout, "New width: %d , and height: %d\n", myWindow.getWindowDimensions().width, myWindow.getWindowDimensions().height);
}

void sendViewMatrixToShaders(glm::mat4 viewMat) {
    particleShader.useShaderProgram();
    glUniformMatrix4fv(glGetUniformLocation(particleShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(viewMat));
    skyboxShader.useShaderProgram();
    glUniformMatrix4fv(glGetUniformLocation(myBasicShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(viewMat));
    myBasicShader.useShaderProgram();
    glUniformMatrix4fv(glGetUniformLocation(myBasicShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(viewMat));
}

void sendFogComputationSignalToShaders(GLint isComputeFog) {
    myBasicShader.useShaderProgram();
    glUniform1i(glGetUniformLocation(myBasicShader.shaderProgram, "isComputeFog"), isComputeFog);
    particleShader.useShaderProgram();
    glUniform1i(glGetUniformLocation(particleShader.shaderProgram, "isComputeFog"), isComputeFog);
}

void keyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mode) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, GL_TRUE);
    }

	if (key >= 0 && key < 1024) {
        if (action == GLFW_PRESS) {
            pressedKeys[key] = true;
        } else if (action == GLFW_RELEASE) {
            pressedKeys[key] = false;
        }
    }
}

double lastX = 400;
double lastY = 300;
bool firstMouse = true;
double sensitivity = 0.1f;
float yaw = 1.5f;
float pitch = 0;
void mouseCallback(GLFWwindow* window, double x_pos, double y_pos) {
    if (firstMouse) {
        lastX = x_pos;
        lastY = y_pos;
        firstMouse = false;
    }

    double x_offset = x_pos - lastX;
    double y_offset = lastY - y_pos;
    lastX = x_pos;
    lastY = y_pos;

    x_offset *= sensitivity;
    y_offset *= sensitivity;

    yaw   += x_offset;
    pitch += y_offset;
    if(pitch > 89.0f)
        pitch = 89.0f;
    if(pitch < -89.0f)
        pitch = -89.0f;

    myCamera.rotate(pitch, yaw);
    view = myCamera.getViewMatrix();
    sendViewMatrixToShaders(view);

    normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
}

void processMovement() {
	if (pressedKeys[GLFW_KEY_W]) {
		myCamera.move(gps::MOVE_FORWARD, cameraSpeed);
		//update view matrix
        view = myCamera.getViewMatrix();
        sendViewMatrixToShaders(view);
        // compute normal matrix for teapot
        normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	}

	if (pressedKeys[GLFW_KEY_S]) {
		myCamera.move(gps::MOVE_BACKWARD, cameraSpeed);
        //update view matrix
        view = myCamera.getViewMatrix();
        sendViewMatrixToShaders(view);
        // compute normal matrix for teapot
        normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	}

	if (pressedKeys[GLFW_KEY_A]) {
		myCamera.move(gps::MOVE_LEFT, cameraSpeed);
        //update view matrix
        view = myCamera.getViewMatrix();
        sendViewMatrixToShaders(view);
        // compute normal matrix for teapot
        normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	}

	if (pressedKeys[GLFW_KEY_D]) {
		myCamera.move(gps::MOVE_RIGHT, cameraSpeed);
        //update view matrix
        view = myCamera.getViewMatrix();
        sendViewMatrixToShaders(view);
        // compute normal matrix for teapot
        normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	}

    if (pressedKeys[GLFW_KEY_Q]) {
        presentMode = true;
//        angle -= 1.0f;
//        // update model matrix for teapot
//        model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0, 1, 0));
//        // update normal matrix for teapot
//        normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
    }

    if (pressedKeys[GLFW_KEY_E]) {
        presentMode = false;
        angle = 1.0f;
        model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0, 1, 0));
        normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
    }

    // WIREFRAME
    if (pressedKeys[GLFW_KEY_P]) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }
    // SOLID
    if (pressedKeys[GLFW_KEY_O]) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }
    // SMOOTH
    if (pressedKeys[GLFW_KEY_U]) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glEnable(GL_POLYGON_SMOOTH);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_DST_ALPHA);
    }
    // POLYGONAL
    if (pressedKeys[GLFW_KEY_Y]) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
    }

    if (pressedKeys[GLFW_KEY_M]) {
        mySkyBox.LoadFromDir(SKYBOX_DAYLIGHT);
        lightingType = LIGHTING_DIRECTIONAL;
        myBasicShader.useShaderProgram();
        glUniform1i(lightingTypeLoc, LIGHTING_DIRECTIONAL);
    }
    if (pressedKeys[GLFW_KEY_N]) {
        mySkyBox.LoadFromDir(SKYBOX_NIGHT);
        lightingType = LIGHTING_POINTLIGHTS;
        myBasicShader.useShaderProgram();
        glUniform1i(lightingTypeLoc, LIGHTING_POINTLIGHTS);
    }
    if (pressedKeys[GLFW_KEY_B]) {
        mySkyBox.LoadFromDir(SKYBOX_NIGHT);
        lightingType = LIGHTING_SPOTLIGHT;
        myBasicShader.useShaderProgram();
        glUniform1i(lightingTypeLoc, LIGHTING_SPOTLIGHT);
    }

    if (pressedKeys[GLFW_KEY_F]) {
        computeFog = 1;
        sendFogComputationSignalToShaders(1);
    }
    if (pressedKeys[GLFW_KEY_G]) {
        computeFog = 0;
        sendFogComputationSignalToShaders(0);
    }

    if (pressedKeys[GLFW_KEY_Z]) {
        lightDir = glm::mat3(glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0.0f, 1.0f, 0.0f))) * lightDir;

        myBasicShader.useShaderProgram();
        glUniform3fv(lightDirLoc, 1, glm::value_ptr(glm::mat3(view) * lightDir));
        particleShader.useShaderProgram();
        glUniform3fv(glGetUniformLocation(particleShader.shaderProgram, "lightDir"), 1, glm::value_ptr(glm::mat3(view) * lightDir));
    }
    if (pressedKeys[GLFW_KEY_X]) {
        lightDir = glm::mat3(glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0.0f, 1.0f, 0.0f))) * lightDir;

        myBasicShader.useShaderProgram();
        glUniform3fv(lightDirLoc, 1, glm::value_ptr(glm::mat3(view) * lightDir));
        particleShader.useShaderProgram();
        glUniform3fv(glGetUniformLocation(particleShader.shaderProgram, "lightDir"), 1, glm::value_ptr(glm::mat3(view) * lightDir));
    }

    if (pressedKeys[GLFW_KEY_R]) {
        drawBoundingBox = true;
    }
    if (pressedKeys[GLFW_KEY_T]) {
        drawBoundingBox = false;
    }
}

void initOpenGLWindow() {
    myWindow.Create(1024, 768, "OpenGL Project");
}

void setWindowCallbacks() {
	glfwSetWindowSizeCallback(myWindow.getWindow(), windowResizeCallback);
    glfwSetKeyCallback(myWindow.getWindow(), keyboardCallback);
    glfwSetCursorPosCallback(myWindow.getWindow(), mouseCallback);
}

void initOpenGLState() {
	glClearColor(0.7f, 0.7f, 0.7f, 1.0f);
	glViewport(0, 0, myWindow.getWindowDimensions().width, myWindow.getWindowDimensions().height);
    glEnable(GL_FRAMEBUFFER_SRGB);
	glEnable(GL_DEPTH_TEST); // enable depth-testing
	glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"
	glEnable(GL_CULL_FACE); // cull face
	glCullFace(GL_BACK); // cull back face
	glFrontFace(GL_CCW); // GL_CCW for counter clock-wise
}

void initCamera() {
    myCamera = gps::Camera(
            glm::vec3(0.0f, 2.0f, 6.0f),
            glm::vec3(0.0f, 0.0f, -10.0f),
            glm::vec3(0.0f, 1.0f, 0.0f),
            boundingBox
    );
}

void initModels() {
    scene.LoadModel("models/scene-no-dor/house-cluster.obj");
    dor.LoadModel("models/house-dor/house_dor.obj");
//    scene.LoadModel("models/scene-1/house_cluster.obj");
    planet.LoadModel("models/planet/Golgotha.obj");

    mySkyBox.LoadFromDir(SKYBOX_DAYLIGHT);

    boundingBox.LoadModel("models/bounding-boxes/binding_box.obj");
    boundingBoxMode.LoadModel("models/bounding-boxes/binding_box.obj");
    particle.LoadModel("models/particle/untitled.obj");
}

void initShaders() {
	myBasicShader.loadShader("shaders/basic.vert", "shaders/basic.frag");
    skyboxShader.loadShader("shaders/skyboxShader.vert", "shaders/skyboxShader.frag");
    depthMapShader.loadShader("shaders/depthMap.vert", "shaders/depthMap.frag");

    particleShader.loadShader("shaders/quad.vert", "shaders/quad.frag");
}

static inline GLint getGlUniformLocation(gps::Shader shader, const std::string &uniformName) {
    return glGetUniformLocation(shader.shaderProgram, uniformName.c_str());
}

void initShadowFBO() {
    glGenFramebuffers(1, &shadowMapFBO);

    glGenTextures(1, &depthMapTexture);
    glBindTexture(GL_TEXTURE_2D, depthMapTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,
                 SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

    glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMapTexture, 0);

    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

static glm::mat4 lightSpaceTransform() {
    glm::mat4 lightView = glm::lookAt(lightDir, glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    const GLfloat near_plane = 0.1f, far_plane = 20.0f;
    glm::mat4 lightProjection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, near_plane, far_plane);
    return lightProjection * lightView;
}

void initParticleShaderUniforms() {
    particleShader.useShaderProgram();
    glUniformMatrix4fv(glGetUniformLocation(particleShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
    glUniformMatrix4fv(glGetUniformLocation(particleShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(glGetUniformLocation(particleShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

    glUniform3fv(glGetUniformLocation(particleShader.shaderProgram, "lightDir"), 1, glm::value_ptr(glm::mat3(view) * lightDir));
    glUniform3fv(glGetUniformLocation(particleShader.shaderProgram, "lightColor"), 1, glm::value_ptr(lightColor));

    glUniform3fv(getGlUniformLocation(particleShader, "spotLightColor"), 1, glm::value_ptr(spotLightColor));
    glUniform1f(getGlUniformLocation(particleShader, "spotLightConst"), spotLightConst);
    glUniform1f(getGlUniformLocation(particleShader, "spotLightLinear"), spotLightLinear);
    glUniform1f(getGlUniformLocation(particleShader, "spotLightQuadratic"), spotLightQuadratic);
    glUniform1f(getGlUniformLocation(particleShader, "spotCutoff"), spotCutoff);
    glUniform1f(getGlUniformLocation(particleShader, "spotOuterCutoff"), spotOuterCutoff);
}

void initUniforms() {
	myBasicShader.useShaderProgram();

    // create model matrix for teapot
    model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0.0f, 1.0f, 0.0f));
	modelLoc = glGetUniformLocation(myBasicShader.shaderProgram, "model");

	// get view matrix for current camera
	view = myCamera.getViewMatrix();
	viewLoc = glGetUniformLocation(myBasicShader.shaderProgram, "view");
	// send view matrix to shader
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

    // compute normal matrix for teapot
    normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	normalMatrixLoc = glGetUniformLocation(myBasicShader.shaderProgram, "normalMatrix");

	// create projection matrix
	projection = glm::perspective(glm::radians(45.0f),
                               (float)myWindow.getWindowDimensions().width / (float)myWindow.getWindowDimensions().height,
                               0.1f, 50.0f);
	projectionLoc = glGetUniformLocation(myBasicShader.shaderProgram, "projection");
	// send projection matrix to shader
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));	

	//set the light direction (direction towards the light)
	lightDir = glm::vec3(0.0f, 1.0f, 1.0f);
	lightDirLoc = glGetUniformLocation(myBasicShader.shaderProgram, "lightDir");
	// send light dir to shader
	glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDir));

	//set light color
	lightColor = glm::vec3(1.0f, 1.0f, 1.0f); //white light
	lightColorLoc = glGetUniformLocation(myBasicShader.shaderProgram, "lightColor");
	// send light color to shader
	glUniform3fv(lightColorLoc, 1, glm::value_ptr(lightColor));

    for (int i = 0; i < LEN_ARR(pointLightPositions); i++) {
        std::string no = std::to_string(i);
        glUniform3fv(getGlUniformLocation(myBasicShader, "pointLightPositions[" + no + "]"), 1, glm::value_ptr(pointLightPositions[i]));
        glUniform3fv(getGlUniformLocation(myBasicShader, "pointLightColors[" + no + "]"), 1, glm::value_ptr(pointLightColors[i]));
        glUniform1f(getGlUniformLocation(myBasicShader, "pointLightConsts[" + no + "]"), pointLightConsts[i]);
        glUniform1f(getGlUniformLocation(myBasicShader, "pointLightLinears[" + no + "]"), pointLightLinears[i]);
        glUniform1f(getGlUniformLocation(myBasicShader, "pointLightQuadratics[" + no + "]"), pointLightQuadratics[i]);
    }

    glUniform3fv(getGlUniformLocation(myBasicShader, "spotLightColor"), 1, glm::value_ptr(spotLightColor));
    glUniform1f(getGlUniformLocation(myBasicShader, "spotLightConst"), spotLightConst);
    glUniform1f(getGlUniformLocation(myBasicShader, "spotLightLinear"), spotLightLinear);
    glUniform1f(getGlUniformLocation(myBasicShader, "spotLightQuadratic"), spotLightQuadratic);
    glUniform1f(getGlUniformLocation(myBasicShader, "spotCutoff"), spotCutoff);
    glUniform1f(getGlUniformLocation(myBasicShader, "spotOuterCutoff"), spotOuterCutoff);

    spotLightPositionLoc = glGetUniformLocation(myBasicShader.shaderProgram, "spotLightPosition");
    glUniform3fv(spotLightPositionLoc, 1, glm::value_ptr(myCamera.getCameraPosition()));

    spotLightDirectionLoc = glGetUniformLocation(myBasicShader.shaderProgram, "spotLightDirection");
    glUniform3fv(spotLightDirectionLoc, 1, glm::value_ptr(myCamera.getCameraFrontDirection()));

    lightingTypeLoc = glGetUniformLocation(myBasicShader.shaderProgram, "lightingType");
    glUniform1i(lightingTypeLoc, LIGHTING_DIRECTIONAL);

    glUniformMatrix4fv(glGetUniformLocation(myBasicShader.shaderProgram, "lightSpaceTrMatrix"),
                       1,
                       GL_FALSE,
                       glm::value_ptr(lightSpaceTransform())
    );

    skyboxShader.useShaderProgram();
    glUniformMatrix4fv(glGetUniformLocation(skyboxShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(glGetUniformLocation(skyboxShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

    sendFogComputationSignalToShaders(0);
    initParticleShaderUniforms();
}

void renderObject(gps::Model3D &object, gps::Shader shader, glm::mat4 objectModelMatrix, glm::mat3 objectNormalMatrix) {
    shader.useShaderProgram();
    glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(objectModelMatrix));
    glUniformMatrix3fv(glGetUniformLocation(shader.shaderProgram, "normalMatrix"), 1, GL_FALSE, glm::value_ptr(objectNormalMatrix));
    object.Draw(shader);
}

void renderObjectToDepthBuffer(gps::Model3D &object, gps::Shader shader, glm::mat4 objectModelMatrix) {
    shader.useShaderProgram();
    glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(objectModelMatrix));
    object.Draw(shader);
}

static glm::mat4 planetTransform() {
    return glm::rotate(glm::mat4(1.0f), glm::radians(planetRotation), glm::vec3(0.0f, 1.0f, 0.0f))
           * glm::translate(glm::mat4(1.0f), glm::vec3(20.0f, 30.0f, 0.0f))
           * glm::rotate(glm::mat4(1.0f), glm::radians(planetAngle), glm::vec3(1.0f, 0.0f, 0.0f))
           * glm::rotate(glm::mat4(1.0f), glm::radians(-20.0f), glm::vec3(0.0f, 0.0f, 1.0f));
}

static glm::mat4 dorTransform() {
    glm::mat4 md = model;
    md = glm::translate(md, glm::vec3(-17.864132f, 1.805275f, -3.692993f));
    md = glm::rotate(md, glm::radians(dorRotation), glm::vec3(0.0f, 1.0f, 0.0f));
    md = glm::translate(md, glm::vec3(17.864132f, -1.805275f, 3.692993f));
    if (myCamera.isCameraNear(glm::vec3(-17.864132f, 1.805275f, -3.692993f), glm::vec3(5.0f, 0.0f, 5.0f)))
        dorRotation += ((dorRotation <= 45.0f) ? 0.1f : -0.1f);
    else dorRotation += ((dorRotation >= 0.0f) ? -0.1f : 0.1f);
    return md;
}

void renderSceneToShadowDepthBuffer() {
    depthMapShader.useShaderProgram();

    glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
    glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
    glClear(GL_DEPTH_BUFFER_BIT);

    glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "lightSpaceTrMatrix"),
                       1,
                       GL_FALSE,
                       glm::value_ptr(lightSpaceTransform())
    );

    renderObjectToDepthBuffer(scene, depthMapShader, model);
    // TODO: change model after transforms
    renderObjectToDepthBuffer(dor, depthMapShader, model);
    renderObjectToDepthBuffer(planet, depthMapShader, planetTransform());

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void renderScene() {
    renderSceneToShadowDepthBuffer();

    if (presentMode) {
        model *= glm::rotate(glm::mat4(1.0f), glm::radians(angle/1.5f), glm::vec3(0, 1, 0));
    }

    myBasicShader.useShaderProgram();
    glViewport(0, 0, myWindow.getWindowDimensions().width, myWindow.getWindowDimensions().height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, depthMapTexture);
    glUniform1i(glGetUniformLocation(myBasicShader.shaderProgram, "shadowMap"), 3);

    glUniformMatrix4fv(glGetUniformLocation(myBasicShader.shaderProgram, "lightSpaceTrMatrix"),
                       1,
                       GL_FALSE,
                       glm::value_ptr(lightSpaceTransform())
    );

    glUniform3fv(spotLightPositionLoc, 1, glm::value_ptr(myCamera.getCameraPosition()));
    glUniform3fv(spotLightDirectionLoc, 1, glm::value_ptr(myCamera.getCameraFrontDirection()));

    planetRotation += 1.0f;
    if (planetRotation >= 179)
        planetRotation = -179;
    planetAngle += 0.5f;
    if (planetAngle >= 179)
        planetAngle = -179;

    if (!computeFog) {
        mySkyBox.Draw(skyboxShader, view, projection);
    } else {
        glClearColor(0.5, 0.5, 0.5, 1.0);
    }

    //render the scene
    normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
    renderObject(scene, myBasicShader, model, normalMatrix);

    // TODO: change model after transforms
    glm::mat4 dorModelMat = dorTransform();
    renderObject(
            dor,
            myBasicShader,
            dorModelMat,
            glm::mat3(glm::inverseTranspose(view * dorModelMat))
    );
    renderObject(
            planet,
            myBasicShader,
            planetTransform(),
            glm::mat3(glm::inverseTranspose(view * planetTransform()))
    );

    if (drawBoundingBox)
        renderObject(boundingBoxMode, myBasicShader, model, normalMatrix);
    if (lightingType == LIGHTING_DIRECTIONAL)
        for (gps::ModelNormalMatrix mT : particleSystem.computeModelNormalMatrices(view))
            renderObject(particle, particleShader, mT.model, mT.normal);
}

void cleanup() {
    myWindow.Delete();
    //cleanup code for your own data
}

int main(int argc, const char * argv[]) {

    try {
        initOpenGLWindow();
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    initOpenGLState();
	initModels();
	initShaders();
    initCamera();
    initUniforms();
    initShadowFBO();
    setWindowCallbacks();

	glCheckError();
	// application loop
	while (!glfwWindowShouldClose(myWindow.getWindow())) {
        processMovement();
	    renderScene();

        particleSystem.SpawnNewParticles(rand() % 500, 0.05f);

		glfwPollEvents();
		glfwSwapBuffers(myWindow.getWindow());

		glCheckError();
	}

	cleanup();

    return EXIT_SUCCESS;
}
