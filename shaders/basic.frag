#version 410 core

in vec3 fPosition;
in vec3 fNormal;
in vec2 fTexCoords;
in vec4 fPosLightSpace;

out vec4 fColor;

//matrices
uniform mat4 model;
uniform mat4 view;
uniform mat3 normalMatrix;

//fog
uniform int isComputeFog;

//lighting
uniform int lightingType;

uniform vec3 lightDir;
uniform vec3 lightColor;
// textures
uniform sampler2D diffuseTexture;
uniform sampler2D specularTexture;
uniform sampler2D shadowMap;

//pointlights
#define POINTLIGHTS 8
uniform vec3 pointLightPositions[POINTLIGHTS];
uniform vec3 pointLightColors[POINTLIGHTS];
uniform float pointLightConsts[POINTLIGHTS];
uniform float pointLightLinears[POINTLIGHTS];
uniform float pointLightQuadratics[POINTLIGHTS];

//spotlights
uniform vec3 spotLightPosition;
uniform vec3 spotLightDirection;
uniform vec3 spotLightColor;
uniform float spotLightConst;
uniform float spotLightLinear;
uniform float spotLightQuadratic;
uniform float spotCutoff;
uniform float spotOuterCutoff;

//components
float ambientStrength = 0.2f;
float specularStrength = 0.5f;

float computeShadow()
{
    vec3 normalizedCoords = fPosLightSpace.xyz / fPosLightSpace.w;
    normalizedCoords = normalizedCoords * 0.5 + 0.5;
    if (normalizedCoords.z > 1.0f)
        return 0.0f;
    float closestDepth = texture(shadowMap, normalizedCoords.xy).r;
    float currentDepth = normalizedCoords.z;

    float bias = max(0.05f * (1.0f - dot(fNormal, lightDir)), 0.005f);
    float shadow = currentDepth - bias > closestDepth ? 1.0f : 0.0f;
    return shadow;
}

vec3 computeDirLight()
{
    //compute eye space coordinates
    vec4 fPosEye = view * model * vec4(fPosition, 1.0f);
    vec3 normalEye = normalize(normalMatrix * fNormal);

    //normalize light direction
    vec3 lightDirN = vec3(normalize(vec4(lightDir, 0.0f)));

    //compute view direction (in eye coordinates, the viewer is situated at the origin
    vec3 viewDir = normalize(- fPosEye.xyz);

    //compute ambient light
    vec3 ambient = ambientStrength * lightColor * texture(diffuseTexture, fTexCoords).rgb;

    //compute diffuse light
    vec3 diffuse = max(dot(normalEye, lightDirN), 0.0f) * lightColor * texture(diffuseTexture, fTexCoords).rgb;

    //compute specular light
    vec3 reflectDir = reflect(-lightDirN, normalEye);
    float specCoeff = pow(max(dot(viewDir, reflectDir), 0.0f), 32);
    vec3 specular = specularStrength * specCoeff * lightColor * texture(specularTexture, fTexCoords).rgb;

    if (lightingType == 1) {
        float shadow = computeShadow();
        diffuse *= (1.0f - shadow);
        specular *= (1.0f - shadow);
    }

    return min(ambient + diffuse + specular, 1.0f);
}

vec3 computePointLight(vec3 lightPos, vec3 lightColor, float constant, float linear, float quadratic)
{
    float distance = length(lightPos - fPosition);
    float attenuation = 1.0f / (constant + linear * distance + quadratic * distance * distance);

    vec4 fPosEye = view * model * vec4(fPosition, 1.0f);
    vec3 normalEye = normalize(normalMatrix * fNormal);
    vec3 lightDirN = vec3(normalize(view * vec4(lightPos - fPosition, 0.0f)));
    vec3 viewDir = normalize(- fPosEye.xyz);

    vec3 ambient = attenuation * ambientStrength * lightColor * texture(diffuseTexture, fTexCoords).rgb;
    vec3 diffuse = attenuation * max(dot(normalEye, lightDirN), 0.0f) * lightColor * texture(diffuseTexture, fTexCoords).rgb;

    vec3 reflectDir = reflect(-lightDirN, normalEye);
    float specCoeff = pow(max(dot(viewDir, reflectDir), 0.0f), 32);
    vec3 specular = attenuation * specularStrength * specCoeff * lightColor * texture(specularTexture, fTexCoords).rgb;

    return min(ambient + diffuse + specular, 1.0f);
}

vec3 computeSpotLight(vec3 lightPos, vec3 lightDirection, vec3 lightCol, float constant, float linear, float quadratic, float cutoff, float outerCutoff)
{
    float distance = length(lightPos - fPosition);
    float attenuation = 1.0f / (constant + linear * distance + quadratic * distance * distance);

    vec4 fPosEye = view * model * vec4(fPosition, 1.0f);
    vec3 normalEye = normalize(normalMatrix * fNormal);
    vec3 lightDirN = vec3(normalize(view * vec4((lightPos - fPosition), 0.0f)));
    vec3 viewDir = normalize(- fPosEye.xyz);

    vec3 reflectDir = reflect(-lightDirN, normalEye);
    float specCoeff = pow(max(dot(viewDir, reflectDir), 0.0f), 32);

    float theta     = dot(lightDirN, normalize(-lightDirection));
    float epsilon   = cutoff - outerCutoff;
    float intensity = clamp((theta - outerCutoff) / epsilon, 0.0, 1.0);

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    if (theta > cutoff) {
        ambient = (attenuation * ambientStrength * lightCol * texture(diffuseTexture, fTexCoords).rgb);
        diffuse = (intensity * attenuation * max(dot(normalEye, lightDirN), 0.0f) * lightCol * texture(diffuseTexture, fTexCoords).rgb);
        specular = (intensity * attenuation * specularStrength * specCoeff * lightCol * texture(specularTexture, fTexCoords).rgb);
        return min(ambient + diffuse + specular, 1.0f);
    }
    return vec3((ambientStrength/4) * texture(diffuseTexture, fTexCoords).rgb);
}

float computeFog()
{
    vec4 fPosEye = view * model * vec4(fPosition, 1.0f);
    float fogDensity = 0.05f;
    float fragmentDistance = length(fPosEye);
    float fogFactor = exp(-pow(fragmentDistance * fogDensity, 2));
    return clamp(fogFactor, 0.0f, 1.0f);
}

void main() 
{
    vec3 color;
    if (lightingType == 1) {
        color = computeDirLight();
    } else if (lightingType == 2) {
        color = computeDirLight() / 100;
        for (int i = 0; i < POINTLIGHTS; i++)
            color += computePointLight(pointLightPositions[i], pointLightColors[i], pointLightConsts[i], pointLightLinears[i], pointLightQuadratics[i]);
    } else if (lightingType == 3) {
        color = computeSpotLight(spotLightPosition, spotLightDirection, spotLightColor, spotLightConst, spotLightLinear, spotLightQuadratic, spotCutoff, spotOuterCutoff);
    }

    vec4 normColor = vec4(color, 1.0f);
    fColor = normColor;

    if (isComputeFog == 1) {
        float fogFactor = computeFog();
        vec4 fogColor = vec4(0.5f, 0.5f, 0.5f, 1.0f);
        fColor = mix(fogColor, normColor, fogFactor);
    }
}
