#version 410 core

layout(location=0) in vec3 vPosition;
layout(location=1) in vec3 vNormal;
layout(location=2) in vec2 vTexCoords;

out vec3 fPosition;
out vec3 fNormal;
out vec2 fTexCoords;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

mat4 removeRotation(mat4 v, mat4 m)
{
    mat4 mv = v * m;
    mv[0][0] = 1;
    mv[0][1] = 0;
    mv[0][2] = 0;
    mv[1][0] = 0;
    mv[1][1] = 1;
    mv[1][2] = 0;
    mv[2][0] = 0;
    mv[2][1] = 0;
    mv[2][2] = 1;
    return mv;
}

void main()
{
    gl_Position = projection * removeRotation(view, model) * vec4(vPosition, 1.0f);
    fPosition = vPosition;
    fNormal = vNormal;
    fTexCoords = vTexCoords;
}
